class Withdrawal < ActiveRecord::Base
  belongs_to :user
  attr_accessible :amount, :iban, :user
end
