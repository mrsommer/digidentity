class User < ActiveRecord::Base
  has_many :withdrawals
  has_many :deposits
  has_many :inbound_transactions, :class_name => 'Transaction', :foreign_key => 'recipient_id'
  has_many :outbound_transactions, :class_name => 'Transaction', :foreign_key => 'sender_id'
  attr_accessible :name, :password
  attr_accessor :password
  before_save :hash_new_password, :if => :password_changed?
  def saldo
    total = 0.0
    self.deposits.each do |d|
      total += d.amount
    end
    self.withdrawals.each do |w|
      total -= w.amount
    end
    self.inbound_transactions.each do |i|
      total += i.amount
    end
    self.outbound_transactions.each do |o|
      total -= o.amount
    end
    return total
  end
  def hash_new_password
    self.password_hashed = Digest::Whirlpool::hexdigest(@password.unpack('C*').zip(ENV['authsalt'].unpack('C*')).map{ |a,b| a ^ b }.pack('C*'))
  end
  
  def password_changed?
    !@password.blank?
  end
  
  def self.authenticate(name, password)
    if user = find_by_name(name)
      if Digest::Whirlpool::hexdigest(password.unpack('C*').zip(ENV['authsalt'].unpack('C*')).map{ |a,b| a ^ b }.pack('C*')) == user.password_hashed
        return user
      end
    end
    return nil
  end
end
