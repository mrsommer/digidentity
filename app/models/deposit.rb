class Deposit < ActiveRecord::Base
  belongs_to :user
  attr_accessible :amount, :user, :iban
end
