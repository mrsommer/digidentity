class AccountController < ApplicationController
  before_filter :login_required
  
  def transfer
    if request.post?
      amount = params[:amt].to_f
      if amount > @user.saldo
        @error = 'Opgegeven bedrag groter dan beschikbaar saldo'
      else
        Transaction.create(
          amount: amount,
          sender: @user,
          recipient: User.find(params[:recipient].to_i)
        )
        redirect_to '/transfer'
      end
    end
  end
end
