class AuthController < ApplicationController
  def login
    if request.post?
      if u = User.authenticate(params[:name],params[:password])
        session[:user] = u.id
        redirect_to '/'
      end
    end
  end
  def logout
    reset_session
    redirect_to '/'
  end
end
