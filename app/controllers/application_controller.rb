class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_vars
  def set_vars
    @user = User.find_by_id(session[:user])
  end
  def login_required
    if @user == nil
      redirect_to '/login'
    end
  end
end
