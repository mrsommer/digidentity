DigidentityBank::Application.routes.draw do
  match 'deposit', :to => 'account#deposit', :via => [ :get, :post ]
  match 'transfer', :to => 'account#transfer', :via => [ :get, :post ]
  match 'logout', :to => 'auth#logout', :via => [ :get ]
  match 'login', :to => 'auth#login', :via => [ :get, :post ]
  root :to => 'page#index'
end
