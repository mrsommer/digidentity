require 'test_helper'

class AccountControllerTest < ActionController::TestCase
  test "should get transfer" do
    get :transfer
    assert_response :success
  end

  test "should get deposit" do
    get :deposit
    assert_response :success
  end

end
