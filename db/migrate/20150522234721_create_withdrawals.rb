class CreateWithdrawals < ActiveRecord::Migration
  def change
    create_table :withdrawals do |t|
      t.references :user
      t.string :iban
      t.decimal :amount, :precision => 13, :scale => 2

      t.timestamps
    end
    add_index :withdrawals, :user_id
  end
end
