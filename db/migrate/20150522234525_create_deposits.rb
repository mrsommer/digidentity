class CreateDeposits < ActiveRecord::Migration
  def change
    create_table :deposits do |t|
      t.references :user
      t.decimal :amount, :precision => 13, :scale => 2
      t.string :iban

      t.timestamps
    end
    add_index :deposits, :user_id
  end
end
