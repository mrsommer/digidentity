class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :sender
      t.references :recipient
      t.decimal :amount, :precision => 13, :scale => 2

      t.timestamps
    end
    add_index :transactions, :sender_id
    add_index :transactions, :recipient_id
  end
end
