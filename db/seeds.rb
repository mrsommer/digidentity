u1 = User.create(
  name: 'John Doe',
  password: 'abc123'
)
u2 = User.create(
  name: 'Jane Doe',
  password: 'abc123'
)
Deposit.create([{
  iban: 'NLRABO1234567890',
  amount: 35.00,
  user: u1
},{
  iban: 'NLRABO1234567890',
  amount: 84.95,
  user: u1
},{
  iban: 'NLRABO1234567890',
  amount: 132.23,
  user: u1
},{
  iban: 'NLRABO1234567890',
  amount: 65.00,
  user: u2
},{
  iban: 'NLRABO1234567890',
  amount: 54.95,
  user: u2
},{
  iban: 'NLRABO1234567890',
  amount: 172.23,
  user: u2
}])