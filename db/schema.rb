# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20150522234721) do

  create_table "deposits", :force => true do |t|
    t.integer  "user_id"
    t.decimal  "amount"
    t.string   "iban"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "deposits", ["user_id"], :name => "index_deposits_on_user_id"

  create_table "transactions", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.decimal  "amount"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "transactions", ["recipient_id"], :name => "index_transactions_on_recipient_id"
  add_index "transactions", ["sender_id"], :name => "index_transactions_on_sender_id"

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "password_hashed"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "withdrawals", :force => true do |t|
    t.integer  "user_id"
    t.string   "iban"
    t.decimal  "amount"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "withdrawals", ["user_id"], :name => "index_withdrawals_on_user_id"

end
